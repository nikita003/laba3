package org.grechishnikov.laba2;

public interface Receiver {
    public String getMessage(String message);
}
