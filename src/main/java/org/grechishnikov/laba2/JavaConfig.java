package org.grechishnikov.laba2;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan ("org.grechishnikov.laba2")
public class JavaConfig {
}
